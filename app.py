from flask import Flask, Response, request, jsonify
from flask_httpauth import HTTPBasicAuth
import threading
import time
import json

app = Flask(__name__)
auth = HTTPBasicAuth()
data = None
users = {"ezpass": "ezpass@2023"}


def send_heartbeat():
    global data
    while True:
        time.sleep(10)
        data = {"heartbeat": True}


heartbeat_thread = threading.Thread(target=send_heartbeat)
heartbeat_thread.daemon = True
heartbeat_thread.start()


def event_stream():
    global data

    while True:
        if data:
            yield "data: {}\n\n".format(json.dumps(data))
            data = None
        time.sleep(1)


@auth.verify_password
def verify_password(username, password):
    if username in users and users[username] == password:
        return username


@app.route("/events")
@auth.login_required
def events():
    return Response(event_stream(), content_type="text/event-stream")


@app.route("/data", methods=["POST"])
@auth.login_required
def set_data():
    global data
    data = request.json
    return jsonify({"message": "Data received successfully"})


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0")
