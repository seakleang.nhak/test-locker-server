import requests


def event_listener():
    response = requests.get("http://localhost:5000/events", stream=True)
    for line in response.iter_lines():
        if line:
            print("Received event:", line.decode("utf-8"))


if __name__ == "__main__":
    event_listener()
